<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Category;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function index()
    {
        return Application::all();
    }

    public function getFilteredObjects(Request $request) {
        $category = $request->get('category');
        $platforms = $request->get('platforms');
        $categories = $platforms;
        array_push($categories, $category);
        $categoryIds = Category::whereIn('title', $categories)->pluck('id');
        // Применяем фильтры к вашим объектам
        return Application::whereHas('categories', function ($query) use ($categoryIds) {
            $query->whereIn('category_id', $categoryIds);
        }, '=', count($categoryIds))->get();
    }
}
