# epic-partners



## Инструкция по развертыванию

```
1. Запустить docker
2. Запустить команду docker-compose up, дождаться её завершения
3. Убедиться, что в докере запущен проект, и зайти в контейнер докера командой docker-compose exec web bash
4. Проверить версию php: php -v (не ниже 8.1)
4. Запустить команду php composer.phar install
5. Убедиться, что создался файл .env (если нет, сделать копию от .env.example) и сгенерировать ключ php artisan key:generate
6. Проверить доступы к базе данных и настройки pusher

DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=laravel_epic-partners
DB_USERNAME=root
DB_PASSWORD=root
BROADCAST_DRIVER=pusher

7. Запустить команду php artisan migrate --seed
8. Отдельной вкладкой в консоли установить npm install и запустить npm run dev или npm run build
9. Зарегистрировать аккаунт и перейти на вкладку Objects
```

## Используемые пакеты

<pre>
Laravel Jetstream with Inertia
Vue.js
</pre>
