<?php

namespace Database\Seeders;

use App\Models\Application;
use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Создание категорий
        $categories = [
            ['title' => 'Платные'],
            ['title' => 'Бесплатные'],
            ['title' => 'Android'],
            ['title' => 'iOS'],
        ];

        Category::insert($categories);
    }
}
