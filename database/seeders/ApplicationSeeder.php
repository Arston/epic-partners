<?php

namespace Database\Seeders;

use App\Models\Application;
use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $objects = [
            [
                'title' => 'Приложение 1',
                'categories' => ['Платные', 'Android'],
            ],
            [
                'title' => 'Приложение 2',
                'categories' => ['Бесплатные', 'Android'],
            ],
            [
                'title' => 'Приложение 3',
                'categories' => ['Платные', 'iOS'],
            ],
            [
                'title' => 'Приложение 4',
                'categories' => ['Бесплатные', 'iOS'],
            ],
            [
                'title' => 'Приложение 5',
                'categories' => ['Платные', 'Android', 'iOS'],
            ],
        ];

        foreach ($objects as $objectData) {
            $application = Application::create(['title' => $objectData['title']]);

            $categories = Category::whereIn('title', $objectData['categories'])->get();

            $application->categories()->attach($categories);
        }
    }
}
